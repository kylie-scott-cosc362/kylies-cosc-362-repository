#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int numRows;
    int numCols;
    int imageSize;
    int row, col;
    unsigned char *outImage;
    unsigned char *ptr;
    FILE *outputFP;

    printf("==============================\n");
    printf("I'm making an image\n");
    printf("==============================\n");

    if(argc != 4)
    {
        printf("Usage: ./RedBluePPM OutFileName rowsize colsize \n");
        exit(1);
    }
    if((numRows = atoi(argv[2])) <= 0)
    {
        printf("Error: number of Rows needs to be positivev. \n");
    }
    if((numCols = atoi(argv[3])) <= 0)
    {
        printf("Error: number of Cols needs to be positive. \n");
    }

    imageSize = numRows * numCols * 3;
    outImage = (unsigned char *)malloc(imageSize);

    if((outputFP = fopen(argv[1], "w")) == NULL)
    {
        perror("output open error");
        printf("Error: can not open the file. \n");
        exit(1);
    }

    ptr = outImage;
    for(row = 0; row < numRows; row++)
    {
        for(col = 0; col < numCols; col++)
        {
            if(col < numCols/2)
            {
                *ptr = 255;
                *(ptr + 1) = 0;
                *(ptr + 2) = 0;
            }
            else
            {
                *ptr = 0;
                *(ptr + 1) = 0;
                *(ptr + 2) = 255;
            }

            ptr += 3;
        }
    }

    fprintf(outputFP, "P6 %d %d 255\n, numCols, numRows");
    fwrite(outImage, 1, imageSize, outputFP);

    fclose(outputFP);

    return 0;
}