import sys
import os
import math
from string import whitespace

def sumer():
	nums = []
	print("Enter values to sum (q to quit): ")
	
	i = ''
	while i != 'q' and i != 'Q':
		i = input()
		if i.isdigit():
			nums.append(int(i))
	
	return sum(nums)

def decodeAdder():
	decode = input("Enter 2 numbers seperated by spaces to add (add 'S' flag to subtract): ")
	if decode.endswith("s") or decode.endswith("S"):
		decode = decode.replace('s', '')
		decode = decode.replace('S', '')
		decoded = decode.split(" ")
		return adder(int(decoded[0]), int(decoded[1]), False)
	else:
		decoded = decode.split(" ")
		return adder(int(decoded[0]), int(decoded[1]))
	  
def adder(num1, num2, add = True):
	if add:
		print(num1, "+", num2, "=", num1 + num2)
	else:
		print(num1, "-", num2, "=", num1 - num2)

def printNames():
	name = "Kylie"
	entry = input("Enter name to print or a number to print the defalt name: ")
	if not entry.isdigit():
		name = entry
		entry = input("Enter number of times to print: ")
	
	for x in range(int(entry)):
		print(name)

def timeSheet():
	TimeSheetData= open("timeSheet.txt","r")
	TimesList = []
	totalH = 0
	totalM = 0
	
	for line in TimeSheetData.readlines():
		if line.find(",") != -1:
			name = line
			continue
		Day, Hour, Min = map(str, line.split(" "))
		TimeInfo = TimeSheet(Day, Hour, Min)
		TimesList.append(TimeInfo)
	
	for x in TimesList:
		totalH += x.retHrs()
		totalM += x.retMins()
	
	time = TimeSheet.timeFormat(totalH, totalM)
	
	print("Total hours for " + name, time)
		
class TimeSheet:
	def __init__(self, day, hrs, mins):
		self.hrs = int(hrs.replace('h', ''))
		self.mins = int(mins.replace('m', ''))
		self.day  = day
	
	def retHrs(self):
		return self.hrs
		
	def retMins(self):
		return self.mins
	
	def timeFormat(hrs, mins):
		while mins >= 60:
			hrs += 1
			mins -= 60
		return str(hrs) + " hours " + str(mins) + " minutes"
