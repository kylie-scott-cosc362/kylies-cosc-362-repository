from LetterFunctions import *

"""
    This is a script to do a small mail merge to make custom letters to magic beings.
    To Run:
    [user@maachine directory]$ python MainLetterScript.py <CSV File Address List>
"""

# ############################################################
# READ IN COMMAND LINE ARGUMENTS
# ############################################################
if len(sys.argv) != 2:
    print("To Run: python MainLetterScript.py AddressList.csv")
else:
    print("Using addresses from the file", str(sys.argv[1]))
    dataFile = str(sys.argv[1])
    
# ############################################################
# OPEN FILES
# ############################################################
AddressData = open(dataFile, "r")

# ############################################################
# READ DATA FROM FILE LINE BY LINE
# ############################################################
print("Reading from file " + dataFile)

AddressList = []
for line in AddressData.readlines():
    First, Last, Address1, Address2 = map(str, line.split(","))
    CurrentAddress = Address(First, Last, Address1, Address2)
    AddressList.append(CurrentAddress)
    
# ############################################################
# WRITE OUT THE FORM LETTER
# ############################################################
for item in range(len(AddressList)):
    Letter = DocumentText(AddressList[item])
    Letter.WriteLetter()

# ############################################################
# COMPILE ALL THE LATEX WITH A BBASH SCRIPT
# ############################################################
os.system("./CompileTex.sh")
print("DONE")
