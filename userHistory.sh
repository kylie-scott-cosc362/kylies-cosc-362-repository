#!/bin/bash -i

y=`date +%Y`
m=`date +%m`
d=`date +%d`
h=`date +%H`
min=`date +%M`
s=`date +%S`

echo "Log file for user:$USER created at $h:$min:$s on $y-$m-$d" > hist.log
echo " " >> hist.log
history >> hist.log
